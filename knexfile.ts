import { knexSnakeCaseMappers } from 'objection';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

module.exports = {
    client: 'mysql2',
    connection: {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
    },
    migrations: {
        directory: './src/modules/db/migrations',
        extension: 'ts',
        stub: './src/modules/db/stubs/migration.stub',
    },
    seeds: {
        directory: './src/modules/db/seeds',
    },
    ...knexSnakeCaseMappers(),
};
