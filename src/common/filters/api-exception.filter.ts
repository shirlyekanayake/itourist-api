import { BaseException } from '../exceptions/base.exception';

import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';

@Catch(BaseException)
export class ApiExceptionFilter implements ExceptionFilter {
    catch(exception: BaseException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const status = exception.getStatus();
        const message = exception.message;
        const options = exception.options;
        const error = exception.name;

        response.status(status).json({
            message,
            result: null,
            statusCode: status,
            errors: options.errors || [
                {
                    key: options.resource || error,
                    constraints: [message],
                },
            ],
        });
    }
}
