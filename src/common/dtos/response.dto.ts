import { HttpException } from '@nestjs/common';

export class ResponseDto<TData> {
    /**
     *
     * response data
     * @type {( [] | TData[])}
     * @memberof ResponseDto
     */
    result: TData;

    /**
     *
     * response message
     *
     * @type {string}
     * @memberof ResponseDto
     */

    message: string;
    /**
     * errors of the response
     *
     * @type {HttpException[]}
     * @memberof ResponseDto
     */
    errors?: HttpException[];

    /**
     * metadata of the paginated response
     *
     * @type {{
     *         total: number;
     *         limit: number;
     *         offset: number;
     *     }}
     * @memberof ResponseDto
     */
    meta?: {
        total: number;
        limit: number;
        offset: number;
    };
}
