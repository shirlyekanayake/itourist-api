import { UnauthorizedException } from './../exceptions/general/unauthorized.exception';
import { User } from './../../modules/user/models/user.model';
import { Reflector } from '@nestjs/core';
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ROLE_KEY } from '../consts';
import { Roles } from '../enums';
import { Request } from 'express';

@Injectable()
export class RoleGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) {}

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const requiredRole = this.reflector.getAllAndOverride<Roles>(ROLE_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);

        if (!requiredRole) return true;

        const { user } = context.switchToHttp().getRequest();

        if (user.role !== requiredRole) throw new UnauthorizedException();
        return true;
    }
}
