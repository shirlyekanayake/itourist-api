import { UnauthorizedException } from './../exceptions/general/unauthorized.exception';
import { AuthService } from './../../modules/auth/auth.service';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Request } from 'express';
import { Reflector } from '@nestjs/core';
import { IS_PUBLIC_KEY } from '../consts';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly reflector: Reflector,
    ) {}

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        try {
            const isPublic = this.reflector.getAllAndOverride<boolean>(
                IS_PUBLIC_KEY,
                [context.getHandler(), context.getClass()],
            );
            if (isPublic) {
                return true;
            }
            const req = context.switchToHttp().getRequest<Request>();
            return this.validateRequest(req);
        } catch (error) {
            return false;
        }
    }

    async validateRequest(req: Request): Promise<boolean> {
        const authHeader = req.header('authorization');

        if (!authHeader) throw new UnauthorizedException();

        const [authMethod, token] = authHeader.split(' ');

        if (authMethod !== 'Bearer') throw new UnauthorizedException();

        const user = await this.authService.authUsingAccessToken(token);

        if (!user) return false;

        req.user = user;

        return true;
    }
}
