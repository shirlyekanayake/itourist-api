import { BaseException } from '../base.exception';
import { HttpStatus } from '@nestjs/common';

export class UnauthorizedException extends BaseException {
    constructor(message?: string) {
        super(
            message ||
                "You don't have enough permission to perfome this action.",
            HttpStatus.UNAUTHORIZED,
        );
    }
}
