import { BaseException } from '../base.exception';
import { HttpStatus } from '@nestjs/common';

export class ConflictExecption extends BaseException {
    constructor(message?: string, resource?: string) {
        super(message || `resource conflict!`, HttpStatus.CONFLICT, {
            resource,
        });
    }
}
