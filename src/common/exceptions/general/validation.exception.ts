import { BaseException } from '../base.exception';
import { HttpStatus, ValidationError } from '@nestjs/common';
export class ValidationException extends BaseException {
    constructor(errors: ValidationError[]) {
        const modifiedErrors = errors.map((err: ValidationError) => {
            return {
                key: err.property,
                constraints: Object.values(err.constraints),
            };
        });
        super('Validation Failed!', HttpStatus.BAD_REQUEST, {
            errors: modifiedErrors,
        });
    }
}
