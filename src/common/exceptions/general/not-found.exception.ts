import { BaseException } from '../base.exception';
import { HttpStatus } from '@nestjs/common';

export class NotFoundException extends BaseException {
    constructor(resource: string) {
        super(`${resource} not found`, HttpStatus.NOT_FOUND);
    }
}
