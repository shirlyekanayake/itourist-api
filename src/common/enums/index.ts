export enum Roles {
    SUPER_ADMIN = 'super_admin',
    ADMIN = 'admin',
    TOURIST = 'tourist',
}

export enum Genders {
    MALE = 'male',
    FEMALE = 'female',
    OTHER = 'other',
}
