import { Roles } from './../enums/index';
import { SetMetadata } from '@nestjs/common';
import { ROLE_KEY } from '../consts';

export const RequireRole = (role: Roles) => SetMetadata(ROLE_KEY, role);
