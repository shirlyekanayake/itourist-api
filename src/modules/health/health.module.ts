import { TerminusModule } from '@nestjs/terminus';
import { Module } from '@nestjs/common';
import { HealthController } from './health.controller';
import { DatabaseModule } from '../db/database.module';
@Module({
    imports: [DatabaseModule, TerminusModule],
    controllers: [HealthController],
    providers: [],
})
export class HealthModule {}
