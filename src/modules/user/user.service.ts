import { Roles } from './../../common/enums/index';
import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dtos/create-user.dto';
import { User } from './models/user.model';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
    async createUser(
        createUserDto: CreateUserDto,
        options = { role: Roles.TOURIST },
    ): Promise<User> {
        const { password } = createUserDto;
        const hashedPassword = await bcrypt.hash(password, 12);

        const user = User.query()
            .insert({
                ...createUserDto,
                password: hashedPassword,
                role: options.role,
            })
            .modify('defaultSelects');

        return user;
    }

    async findUserByEmail(
        email: string,
        options = { withPassword: false },
    ): Promise<User | undefined> {
        let modifier = options.withPassword ? 'withPassword' : 'defaultSelects';
        const user = await User.query()
            .modify(modifier)
            .where('email', email)
            .first();
        return user;
    }

    async findUserById(
        id: string,
        options = { withPassword: false },
    ): Promise<User | undefined> {
        let modifier = options.withPassword ? 'withPassword' : 'defaultSelects';
        const user = await User.query()
            .modify(modifier)
            .where('id', id)
            .first();
        return user;
    }
}
