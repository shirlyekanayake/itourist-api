import { Genders, Roles } from './../../../common/enums/index';
import { Model, QueryContext } from 'objection';
import { UserDto } from '../dtos/user.dto';

export class User extends Model {
    static get tableName() {
        return 'users';
    }

    id!: number;
    email!: string;
    password: string;
    age: number;
    gender: Genders;
    imgUrl?: string;
    role!: Roles;

    createdAt!: Date | string;
    updatedAt!: Date | string;

    $beforeInsert(): void {
        this.createdAt = new Date();
    }

    $beforeUpdate(): void {
        this.updatedAt = new Date();
    }

    $afterInsert() {
        delete this.password;
    }

    isSuperAdmin(): boolean {
        return this.role === Roles.SUPER_ADMIN;
    }

    isAdmin(): boolean {
        return this.role === Roles.ADMIN;
    }

    static modifiers = {
        defaultSelects(query) {
            const { ref } = User;
            query.select(
                ref('id'),
                ref('email'),
                ref('age'),
                ref('gender'),
                ref('imgUrl'),
                ref('role'),
            );
        },
        withPassword(query) {
            const { ref } = User;
            query.select(ref('users.*'));
        },
    };

    toDto(): UserDto {
        return {
            id: this.id,
            age: this.age,
            email: this.email,
            gender: this.gender,
            imgUrl: this.imgUrl,
            role: this.role,
        };
    }
}
