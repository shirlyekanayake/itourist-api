import { RequireRole } from './../../common/decorators/role.decorator';
import { UserService } from './user.service';
import { Controller, Get, Post } from '@nestjs/common';
import { Roles } from 'src/common/enums';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get('/')
    @RequireRole(Roles.SUPER_ADMIN)
    helloSuperAdmin() {
        return 'hellooo';
    }
}
