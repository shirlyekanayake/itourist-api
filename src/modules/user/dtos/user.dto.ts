import { Genders, Roles } from './../../../common/enums/index';
export class UserDto {
    /**
     * User id
     *
     * @example 1122
     * @type {number}
     * @memberof UserDto
     */
    id!: number;

    /**
     * User email
     *
     * @example user@abc.com
     * @type {string}
     * @memberof UserDto
     */
    email!: string;

    /**
     * User age
     *
     * @example 24
     * @type {number}
     * @memberof UserDto
     */
    age: number;

    /**
     * User gender
     *
     * @example Genders.MALE, Genders.FEMALE, Genders.OTHER
     * @type {Genders}
     * @memberof UserDto
     */
    gender: Genders;

    /**
     * Profile image url of the user.
     *
     * @example https://picsum.photos/200
     * @type {string}
     * @memberof UserDto
     */
    imgUrl: string;

    /**
     * Role associated with the user
     *
     * @example Roles.SUPER_ADMIN, Roles.ADMIN, Roles.TRAVELER
     * @type {Roles}
     * @memberof UserDto
     */
    role!: Roles;
}
