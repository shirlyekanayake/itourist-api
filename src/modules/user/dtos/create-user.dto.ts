import {
    IsEmail,
    IsEnum,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
} from 'class-validator';
import { Genders } from './../../../common/enums/index';
export class CreateUserDto {
    /**
     * Name of the user
     *
     * @type {string}
     * @memberof CreateUserDto
     */
    @IsString()
    @IsNotEmpty()
    name!: string;

    /**
     * email of the user
     *
     * @type {string}
     * @memberof CreateUserDto
     */
    @IsEmail()
    @IsNotEmpty()
    email!: string;

    /**
     * password of the user
     *
     * @type {string}
     * @memberof CreateUserDto
     */
    @IsString()
    @IsNotEmpty()
    password!: string;

    /**
     * age of the user
     *
     * @type {number}
     * @memberof CreateUserDto
     */
    @IsNumber()
    @IsNotEmpty()
    age!: number;

    /**
     * gender of the user
     *
     * @type {Genders}
     * @memberof CreateUserDto
     */
    @IsNotEmpty()
    @IsEnum(Genders)
    gender!: Genders;

    /**
     * image url of the user
     *
     * @type {string}
     * @memberof CreateUserDto
     */
    @IsString()
    @IsOptional()
    imgUrl?: string;
}
