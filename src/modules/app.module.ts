import { RoleGuard } from './../common/guards/role.guard';
import { DatabaseModule } from './db/database.module';
import { AuthGuard } from './../common/guards/auth.guard';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import configs from './../configs/config';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { HealthModule } from './health/health.module';
import { APP_GUARD } from 'src/common/consts';

@Module({
    imports: [
        ConfigModule.forRoot({
            load: [configs],
            isGlobal: true,
        }),
        DatabaseModule,
        HealthModule,

        AuthModule,
        UserModule,
    ],
    providers: [
        {
            provide: APP_GUARD,
            useClass: AuthGuard,
        },
        {
            provide: APP_GUARD,
            useClass: RoleGuard,
        },
    ],
})
export class AppModule {}
