import { ConflictExecption } from './../../common/exceptions/general/conflict.exception';
import { UserService } from './../user/user.service';
import { User } from './../user/models/user.model';
import { CreateUserDto } from './../user/dtos/create-user.dto';
import { LoginDto } from './dtos/login.dto';
import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ResponseDto } from 'src/common/dtos/response.dto';
import { AuthService } from './auth.service';
import { AuthDto } from './dtos/auth.dto';
import { Public } from 'src/common/decorators/public.decorator';
import { Roles } from 'src/common/enums';
import { RequireRole } from 'src/common/decorators/role.decorator';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('/login')
    @HttpCode(200)
    @Public()
    async login(@Body() loginDto: LoginDto): Promise<ResponseDto<AuthDto>> {
        const result = await this.authService.loginUser(loginDto);
        return {
            result,
            message: 'loged in successfully',
        };
    }

    @Post('/register')
    @Public()
    async register(
        @Body() createUserDto: CreateUserDto,
    ): Promise<ResponseDto<User>> {
        const result = await this.authService.registerUser(createUserDto);
        return {
            result,
            message: 'User created successfully!',
        };
    }

    @Post('admin/register')
    @RequireRole(Roles.SUPER_ADMIN)
    async registerAdmin(
        @Body() createUserDto: CreateUserDto,
    ): Promise<ResponseDto<User>> {
        const result = await this.authService.registerUser(
            createUserDto,
            Roles.ADMIN,
        );
        return {
            result,
            message: 'Admin user created successfully!',
        };
    }

    @Post('spadmin/register')
    @RequireRole(Roles.SUPER_ADMIN)
    async registerSuperAdmin(
        @Body() createUserDto: CreateUserDto,
    ): Promise<ResponseDto<User>> {
        const result = await this.authService.registerUser(
            createUserDto,
            Roles.SUPER_ADMIN,
        );
        return {
            result,
            message: 'Super admin user created successfully!',
        };
    }
}
