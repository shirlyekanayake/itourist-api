import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
    /**
     * email of the user
     *
     * @type {string}
     * @example 'user@abc.com'
     * @memberof LoginDto
     */
    @IsEmail()
    @IsNotEmpty()
    email: string;

    /**
     * password of the user
     *
     * @type {string}
     * @memberof LoginDto
     */
    @IsNotEmpty()
    @IsString()
    password: string;
}
