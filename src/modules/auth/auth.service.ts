import { Roles } from './../../common/enums/index';
import { CreateUserDto } from './../user/dtos/create-user.dto';
import { UnauthorizedException } from './../../common/exceptions/general/unauthorized.exception';
import { AuthDto } from './dtos/auth.dto';
import { LoginDto } from './dtos/login.dto';
import { UserService } from './../user/user.service';
import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user/models/user.model';
import { ConflictExecption } from 'src/common/exceptions';

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
    ) {}

    async loginUser(data: LoginDto): Promise<AuthDto> {
        const { email, password } = data;
        const user = await this.userService.findUserByEmail(email, {
            withPassword: true,
        });
        if (!user)
            throw new UnauthorizedException('Invalid email or password!');

        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch)
            throw new UnauthorizedException('Invalid email or password!');

        const payload = {
            id: user.id,
        };

        const accessToken = await this.jwtService.signAsync(payload, {
            issuer: 'itourist-api',
            subject: user.id.toString(),
            expiresIn: '1h',
        });

        return {
            accessToken,
            expiresIn: '1h',
        };
    }

    async authUsingAccessToken(token: string): Promise<User> {
        const payload = this.jwtService.decode(token);

        if (!payload) throw new UnauthorizedException();

        const id: string = payload['id'];

        const user = await this.userService.findUserById(id);

        if (!user) throw new UnauthorizedException();

        return user;
    }

    async registerUser(
        createUserDto: CreateUserDto,
        role: Roles = Roles.TOURIST,
    ): Promise<User> {
        const { email } = createUserDto;
        const user = await this.userService.findUserByEmail(email);

        if (user)
            throw new ConflictExecption(
                'An account already exists with this email address.',
                'email',
            );

        const createdUser = await this.userService.createUser(createUserDto, {
            role,
        });
        return createdUser;
    }
}
