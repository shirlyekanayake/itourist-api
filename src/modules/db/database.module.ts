import { Module } from '@nestjs/common';
import { DatabaseHealthIndicator } from './database.health';
import { databaseProvider } from './database.provider';

@Module({
    imports: [],
    controllers: [],
    providers: [...databaseProvider, DatabaseHealthIndicator],
    exports: [...databaseProvider, DatabaseHealthIndicator],
})
export class DatabaseModule {}
