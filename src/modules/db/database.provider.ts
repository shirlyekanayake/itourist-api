import { DatabaseConfig } from '../../configs/config';
import { Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppConfig } from 'src/configs/config';
import { KNEX_CONNECTION } from './consts';
import { knex } from 'knex';
import { knexSnakeCaseMappers, Model } from 'objection';

export const databaseProvider: Provider[] = [
    {
        provide: KNEX_CONNECTION,
        inject: [ConfigService],
        useFactory: (appConfig: ConfigService<AppConfig>) => {
            const database = appConfig.get<DatabaseConfig>('database');
            const isDevEnv = appConfig.get<boolean>('isDevEnv');

            const knexConnectionObj = knex({
                client: 'mysql2',
                connection: {
                    ...database,
                },
                debug: isDevEnv,
                ...knexSnakeCaseMappers(),
            });

            Model.knex(knexConnectionObj);

            return knexConnectionObj;
        },
    },
];
