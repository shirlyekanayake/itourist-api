import { DB_HELTH_KEY, KNEX_CONNECTION } from './consts';
import { Inject, Injectable } from '@nestjs/common';
import {
    HealthCheckError,
    HealthIndicator,
    HealthIndicatorResult,
} from '@nestjs/terminus';
import { Knex } from 'knex';

@Injectable()
export class DatabaseHealthIndicator extends HealthIndicator {
    constructor(@Inject(KNEX_CONNECTION) private knex: Knex) {
        super();
    }

    async isHealthy(): Promise<HealthIndicatorResult> {
        try {
            await this.knex.raw('select 1+1 as result');
            return this.getStatus(DB_HELTH_KEY, true, { status: 'up' });
        } catch (error) {
            throw new HealthCheckError(
                'Database connection failed',
                this.getStatus(DB_HELTH_KEY, true, { status: 'down' }),
            );
        }
    }
}
