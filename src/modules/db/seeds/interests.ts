import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    knex.raw('SET foreign_key_checks = 0;');
    await knex('interests').del();
    knex.raw('SET foreign_key_checks = 1;');
    // Inserts seed entries
    await knex('interests').insert([
        { interestTypeId: 1, name: 'Beach' },
        { interestTypeId: 1, name: 'Water Villa' },
        { interestTypeId: 1, name: 'Eco' },
        { interestTypeId: 1, name: 'Nature' },

        { interestTypeId: 2, name: 'Budget' },
        { interestTypeId: 2, name: 'Luxury Tour' },

        { interestTypeId: 3, name: 'Hiking' },
        { interestTypeId: 3, name: 'Clam & Quiet' },
        { interestTypeId: 3, name: 'Surfing' },
        { interestTypeId: 3, name: 'Climbing' },

        { interestTypeId: 4, name: 'Solo' },
        { interestTypeId: 4, name: 'Couple' },
        { interestTypeId: 4, name: 'Family' },
        { interestTypeId: 4, name: 'Group' },
    ]);
}
