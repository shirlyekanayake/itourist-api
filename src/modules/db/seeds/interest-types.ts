import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries

    knex.raw('SET foreign_key_checks = 0;');
    await knex('interest_types').del();
    knex.raw('SET foreign_key_checks = 1;');

    // Inserts seed entries
    await knex('interest_types').insert([
        { id: 1, name: 'Type of stay' },
        { id: 2, name: 'Search for' },
        { id: 3, name: 'Experience' },
        { id: 4, name: 'Travel Mode' },
    ]);
}
