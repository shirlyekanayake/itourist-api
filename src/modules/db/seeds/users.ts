import { Genders, Roles } from './../../../common/enums/index';
import { Knex } from 'knex';
import { hash } from 'bcrypt';

const superAdminUser = {
    name: 'itourist superadmin',
    age: 25,
    password: '12345678',
    email: 'sadmin@itourist.com',
    gender: Genders.MALE,
    role: Roles.SUPER_ADMIN,
};

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    knex.raw('SET foreign_key_checks = 0;');
    await knex('users').del();
    knex.raw('SET foreign_key_checks = 1;');
    const password = await hash(superAdminUser.password, 10);

    // Inserts seed entries
    await knex('users').insert([{ ...superAdminUser, password }]);
}
