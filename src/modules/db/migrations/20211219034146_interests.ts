import { Knex } from 'knex';

const tableName = 'interests';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.integer('interest_type_id').unsigned().nullable();

        table.foreign('interest_type_id').references('interest_types.id');

        // timestamps
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName);
}
