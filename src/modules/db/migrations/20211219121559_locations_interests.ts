import { Knex } from 'knex';

const tableName = 'locations_interests';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();

        table.integer('location_id').unsigned().nullable();
        table.foreign('location_id').references('locations.id');

        table.integer('interest_id').unsigned().nullable();
        table.foreign('interest_id').references('interests.id');

        // timestamps
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName);
}
