import { Knex } from 'knex';

const tableName = 'locations';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('name').unique().notNullable();
        table.string('description').notNullable();

        // timestamps
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName);
}
