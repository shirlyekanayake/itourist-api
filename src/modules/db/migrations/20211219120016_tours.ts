import { Knex } from 'knex';

const tableName = 'tours';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.date('start_date').notNullable();
        table.date('end_date').notNullable();

        table.integer('tour_type_id').unsigned().nullable();
        table.foreign('tour_type_id').references('tour_types.id');
        // timestamps
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName);
}
