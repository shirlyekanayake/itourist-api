import { Knex } from 'knex';

const tableName = 'users_interests';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();

        table.integer('user_id').unsigned().nullable();
        table.foreign('user_id').references('users.id');

        table.integer('interest_id').unsigned().nullable();
        table.foreign('interest_id').references('interests.id');

        // timestamps
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName);
}
