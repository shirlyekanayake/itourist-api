import { Knex } from 'knex';

const tableName = 'users';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();

        table.string('name').notNullable();
        table.integer('age').notNullable();
        table.string('email').unique().notNullable();
        table.string('password').notNullable();
        table.string('gender').notNullable();
        table.string('img_url').nullable();
        table.string('role').notNullable();

        // timestamps
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName);
}
