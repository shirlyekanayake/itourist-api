import { Knex } from 'knex';

const tableName = 'tour_images';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();

        table.string('img_url').notNullable();

        table.integer('tour_id').unsigned().nullable();
        table.foreign('tour_id').references('tours.id');

        // timestamps
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(tableName);
}
