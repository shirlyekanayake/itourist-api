import { cleanEnv, port, str } from 'envalid';

export interface AppConfig {
    port: number;
    secret: string;
    database: DatabaseConfig;
    isDevEnv: boolean;
}

export interface DatabaseConfig {
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
}

const generateAppConfigs = (): AppConfig => {
    const env = cleanEnv(process.env, {
        SECRET: str(),
        PORT: port({ default: 3000 }),
        DB_HOST: str(),
        DB_PORT: port(),
        DB_USER: str(),
        DB_PASSWORD: str(),
        DB_NAME: str(),
    });

    const config: AppConfig = {
        port: env.PORT,
        secret: env.SECRET,
        database: {
            host: env.DB_HOST,
            port: env.DB_PORT,
            user: env.DB_USER,
            password: env.DB_PASSWORD,
            database: env.DB_NAME,
        },
        isDevEnv: env.isDev,
    };

    return config;
};

export default generateAppConfigs;
