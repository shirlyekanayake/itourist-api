import { AuthGuard } from './common/guards/auth.guard';
import { ValidationException } from './common/exceptions/general/validation.exception';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';
import { ConfigService } from '@nestjs/config';
import { Logger, ValidationPipe } from '@nestjs/common';

import * as helmet from 'helmet';
import { ApiExceptionFilter } from './common/filters/api-exception.filter';
import { AllExceptionsFilter } from './common/filters/all-exception.filter';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    const logger = new Logger();

    const configService = app.get(ConfigService);
    const port = configService.get<number>('port');

    app.use(helmet());
    app.enableCors();

    app.setGlobalPrefix('api');

    app.useGlobalFilters(new AllExceptionsFilter(), new ApiExceptionFilter());

    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            transform: true,
            exceptionFactory: (errors) => new ValidationException(errors),
        }),
    );

    await app.listen(port, () => {
        logger.log(`Application started at localhost:${port} 🚀`);
    });
}
bootstrap();
